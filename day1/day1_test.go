package day1

import "testing"

var sample = sonarSweep{ 199, 200, 208, 210, 200, 207, 240, 269, 260, 263 }
//                        0    1    2    3    4    5    6    7    8    9

func TestPart1(t *testing.T) {
	got := countIncreases(sample)
	want := 7
	if got != want {
		t.Errorf("got %d instead of %d", got, want)
	}
}

func TestSumSlices(t *testing.T) {
	list1 := []int{1,2,3}
	got := sumSlice(list1)
	want := 6
	if got != want {
		t.Errorf("got %d instead of %d", got, want)
	}
}

func TestPart2(t *testing.T) {
	got := countIncreasesWindow(sample, 3)
	want := 5
	if got != want {
		t.Errorf("got %d instead of %d", got, want)
	}
}
