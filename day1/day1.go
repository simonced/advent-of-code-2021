package day1

import (
	// "fmt"
	"adventeofcode2021/utils"
)

type sonarSweep []int


func readInput() sonarSweep {
	input := utils.LoadFileLines("day1/input.txt")
	inputs := utils.ConvertAllStrToInt(input)
	return inputs
}


func countIncreases(s sonarSweep) int {
	found := 0
	var a,b int
	for i := 1; i<len(s); i++ {
		b = s[i-1] // before value
		a = s[i]   // current value
		// print(b, "<", a)
		// println(b<a)
		if b<a {
			found++
		}
	}
	return found
}


func sumSlice(s []int) int {
	sum := 0
	for _,v := range s {
		sum += v
	}
	return sum
}


func countIncreasesWindow(s sonarSweep, w int) int {
	slice_count := len(s)-w
	window := make([]int, 3) // temp slice
	windows := make([]int, slice_count+1) // sum of slices
	var k_e int // end index to slice `s`
	for k := 0; k<=slice_count; k++ {
		k_e = k+w
		window = s[k:k_e]
		windows[k] = sumSlice(window)
	}
	return countIncreases(windows)
}


func SolvePart1() int {
	values := readInput()
	return countIncreases(values)
}


func SolvePart2() int {
	values := readInput()
	return countIncreasesWindow(values, 3)
}
