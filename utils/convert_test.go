package utils

import (
	"strconv"
	"testing"
)

func TestConvertAllStrToInt(t *testing.T) {
	list1 := []string{"1", "123", "435", "-123"}
	got := ConvertAllStrToInt(list1)
	want := []int{1, 123, 435, -123}
	if len(want) != len(got) {
		t.Error("the 2 lists are not even the same length")
	}
	for k, v := range got {
		if testv,err := strconv.Atoi(list1[k]); err!=nil || testv!=v {
			t.Errorf("got %d instead of %d", v, testv)
		}
	}
}
