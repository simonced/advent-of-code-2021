package utils

import (
	"log"
	"os"
	"strings"
)


func ReadFile(f string) string {
	data, err := os.ReadFile(f)
	if err!=nil {
		log.Fatal("Can't read your file bro!")
	}

	return strings.Trim(string(data), " \n")
}


func LoadFileLines(f string) []string {
	data := ReadFile(f)
	datas := strings.Trim(data, " \n")
	lines := strings.Split(datas, "\n")

	return lines
}
