package utils

import (
	"log"
	"strconv"
)

func ConvertAllStrToInt(s []string) []int {
	var ints = make([]int, len(s))
	var err error

	for k,v := range s {
		ints[k], err = strconv.Atoi(v)
		if err != nil {
			log.Fatalf("can't convert %s into int!", v)
		}
	}

	return ints
}
