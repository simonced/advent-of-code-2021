package day2

import (
	"reflect"
	"testing"
)

const sample_instructions = `forward 5
down 5
forward 8
up 3
down 8
forward 2`

var sample_instructions_parsed = []Course{
	{Vector: Forward, Value: 5},
	{Vector: Down, Value: 5},
	{Vector: Forward, Value: 8},
	{Vector: Up, Value: 3},
	{Vector: Down, Value: 8},
	{Vector: Forward, Value: 2},
}


func TestParseCourse(t *testing.T) {
	course_txt := "forward 5"
	wanted := Course{Vector: Forward, Value: 5}
	got := ParseCourse(course_txt)
	if got!=wanted {
		t.Error("go ", got, " instead of ", wanted)
	}

	course_txt = "up 5"
	wanted = Course{Vector: Up, Value: 5}
	got = ParseCourse(course_txt)
	if got!=wanted {
		t.Error("go ", got, " instead of ", wanted)
	}

	course_txt = "down 5"
	wanted = Course{Vector: Down, Value: 5}
	got = ParseCourse(course_txt)
	if got!=wanted {
		t.Error("go ", got, " instead of ", wanted)
	}
}


func TestParseInstructions(t *testing.T) {
	got := ParseInstructions(sample_instructions)
	if reflect.DeepEqual(sample_instructions_parsed, got) == false {
		t.Error("Parsing of instructions failed...")
		t.Error("got ", got)
		t.Error("instead of ", sample_instructions_parsed)
		// TODO add details
	}
}


func TestUpdatePosition(t *testing.T) {
	sub := SubmarinePosition{}
	var testcourse Course

	// testing horizontal (add)
	testcourse = Course{Vector: Forward, Value: 5}
	sub.UpdatePosition(testcourse)
	if sub.H!=5 {
		t.Errorf("Horizontal position is %d, but we expected %d", sub.H, 5)
	}
	if sub.D!=0 {
		t.Errorf("Depth is %d, but it should still be 0", sub.D)
	}

	// testing depth 2/2
	testcourse = Course{Vector: Down, Value: 5}
	sub.UpdatePosition(testcourse)
	if sub.D!=5 {
		t.Errorf("Depth position is %d, but we expected %d", sub.D, 5)
	}
	// testing depth 2/2
	testcourse = Course{Vector: Up, Value: 2}
	sub.UpdatePosition(testcourse)
	if sub.D!=3 {
		t.Errorf("Depth position is %d, but we expected %d", sub.D, 3)
	}
}


func TestUpdatePosition2(t *testing.T) {
	wanted := 900
	got := SolvePart2(sample_instructions)
	if got != wanted {
		t.Errorf("wrong sample solution, got %d instead of %d", got, wanted)
	}
}


func TestSolutionPart1(t *testing.T) {
	wanted := 150
	got := SolvePart1(sample_instructions)
	if got != wanted {
		t.Errorf("wrong solution, got %d instead of %d", got, wanted)
	}
}
