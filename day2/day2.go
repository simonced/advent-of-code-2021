package day2

import (
	"strings"
	"strconv"
)

type Direction int

const (
	Forward Direction = iota
	Up
	Down
)

type Course struct {
	Vector Direction
	Value int
}

type SubmarinePosition struct {
	H int // horizontal
	D int // depth
	A int // aim
}


func ParseCourse(c string) Course {
	var course Course

	parts := strings.Split(c, " ")
	// vector
	switch parts[0] {
		case "forward":
			course.Vector = Forward
		case "up":
			course.Vector = Up
		case "down":
			course.Vector = Down
		// Need a default/error case?
	}
	// value
	value,err := strconv.Atoi(parts[1])
	if err != nil {
		value = 0 // TODO find another way?
	}
	course.Value = value

	return course
}


func ParseInstructions(s string) []Course {
	var courses []Course
	lines := strings.Split(s, "\n")
	for _, line := range lines {
		courses = append(courses, ParseCourse(line))
	}
	return courses
}


func (p *SubmarinePosition)UpdatePosition(c Course) {
	switch vec := c.Vector; vec {
		case Forward:
			p.H += c.Value
		case Up:
			p.D -= c.Value
		case Down:
			p.D += c.Value
	}
}


func (p *SubmarinePosition)UpdatePosition2(c Course) {
	switch vec := c.Vector; vec {
		case Forward:
			p.H += c.Value
			p.D += (p.A * c.Value)
		case Up:
			p.A -= c.Value
		case Down:
			p.A += c.Value
	}
}


func SolvePart1(s string) int {
	sub := SubmarinePosition{}
	course := ParseInstructions(s)

	for _, c := range course {
		sub.UpdatePosition(c)
	}

	return sub.H * sub.D
}


func SolvePart2(s string) int {
	sub := SubmarinePosition{}
	course := ParseInstructions(s)

	for _, c := range course {
		sub.UpdatePosition2(c)
	}

	return sub.H * sub.D
}
