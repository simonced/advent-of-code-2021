package main

import (
	"log"
	"adventeofcode2021/utils"
	// day "adventeofcode2021/day1"
	day "adventeofcode2021/day2"
)


func main() {
	instructions := utils.ReadFile("day2/input.txt")
	solution := day.SolvePart2(instructions)
	log.Println("Solution: ", solution)
}
